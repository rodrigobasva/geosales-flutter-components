import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';

class GSTimePicker extends StatelessWidget {

  GSTimePicker({
    this.width,
    this.padding,
    this.labelText,
  });

  final double width;
  final EdgeInsets padding;
  final String labelText;
  final DateFormat format = DateFormat('HH:mm');

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width ?? 160.0,
      padding: padding,
      child: DateTimeField(
        keyboardType: TextInputType.datetime,
        decoration: InputDecoration(
          labelText: labelText,
          border: OutlineInputBorder(),
          prefixIcon: Icon(Icons.access_time),
        ),
        format: format,
        onShowPicker: (BuildContext context, DateTime currentValue) async {
          final TimeOfDay time = await showTimePicker(
            context: context,
            initialTime: TimeOfDay.fromDateTime(currentValue ?? DateTime.now()),
          );
          return DateTimeField.convert(time);
        },
      )
    );
  }
}