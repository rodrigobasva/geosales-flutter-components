import 'package:flutter/material.dart';
import 'package:unicorndial/unicorndial.dart';

import './gs_menu.dart';

class GSScaffold extends StatelessWidget {

  const GSScaffold({
    @required this.title,
    @required this.body,
    this.backgroundColor,
    this.appBarElevation,
    this.bottomAppBar,
    this.fabColor,
    this.fabIcon,
    this.fabOnPressed,
    this.fabMenuButtons,
    this.withDrawer = true,
  });

  final String title;
  final Widget body;
  final Color backgroundColor;
  final double appBarElevation;
  final Widget bottomAppBar;
  final Color fabColor;

  /// Ícone do FAB.
  /// Usar em conjunto ao [fabOnPressed].
  final IconData fabIcon;

  /// Ação ao pressionar o FAB.
  /// Usar em conjunto ao [fabIcon].
  /// Obs.: [fabMenuButtons] será desconsiderado.
  final Function fabOnPressed;

  /// Quando houver [fabMenuButtons], [fabIcon] 
  /// e [fabOnPressed] não devem ser utilizados.
  final List<UnicornButton> fabMenuButtons;
  final bool withDrawer;

  @override
  Widget build(BuildContext context) {
    return withDrawer == true 
      ? _buildScaffold(context)
      : _buildScaffoldWithoutDrawer(context);
  }

  Widget _buildScaffold(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor,
      appBar: AppBar(
        title: Text(title),
        elevation: appBarElevation,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search),
            color: Colors.white,
            onPressed: () {},
          )
        ],
        bottom: bottomAppBar,
      ),
      drawer: Menu(),
      body: body,
      floatingActionButton: _buildFloatingActionButton(context),
    );
  }

  Widget _buildScaffoldWithoutDrawer(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search),
            color: Colors.white,
            onPressed: () {},
          )
        ],
      ),
      body: body,
      floatingActionButton: _buildFloatingActionButton(context),
    );
  }

  Widget _buildFloatingActionButton(BuildContext context) {
    Widget builderFAB;

    if (fabIcon != null) {
      builderFAB = Builder(
        builder: (BuildContext context) {
          return FloatingActionButton(
            backgroundColor: fabColor ?? Theme.of(context).primaryColor,
            foregroundColor: Colors.white,
            child: Icon(fabIcon),
            onPressed: fabOnPressed,
          );
        }
      );
    } else if (fabMenuButtons != null) {
      builderFAB = Builder(
        builder: (BuildContext context) {
          return UnicornDialer(
            hasBackground: false,
            parentButtonBackground: Theme.of(context).primaryColor,
            orientation: UnicornOrientation.VERTICAL,
            parentButton: Icon(Icons.more_vert),
            finalButtonIcon: Icon(Icons.more_horiz),
            childButtons: fabMenuButtons,
          );
        }
      );
    } else {
      builderFAB = Container();
    }

    return builderFAB;
  }
}