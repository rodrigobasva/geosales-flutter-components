import 'package:flutter/material.dart';
import 'package:unicorndial/unicorndial.dart';

class GSFabMenuButton extends StatelessWidget {

  const GSFabMenuButton({
    @required this.heroTag,
    @required this.backgroundColor,
    @required this.icon,
    @required this.onPressed,
    this.labelText,
  });

  final String heroTag;
  final Color backgroundColor;
  final Icon icon;
  final String labelText;
  final Function onPressed;

  @override
  Widget build(BuildContext context) {
    return UnicornButton(
      labelText: labelText,
      labelBackgroundColor: Colors.black,
      labelColor: Colors.white,
      currentButton: FloatingActionButton(
        heroTag: heroTag,
        backgroundColor: backgroundColor,
        mini: true,
        child: icon,
        onPressed: onPressed,
      ),
    );
  }
}
