import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';

import '../utils/gs_field_type.dart';

const String _maskCep = '00.000-000';
const String _maskCpf = '000.000.000-00';
const String _maskCnpj = '00.000.000/0000-00';
const String _maskPhone = '(00) 00000-0000';

class GSMaskedField extends StatelessWidget {

  const GSMaskedField({
    @required this.fieldType,
    this.width,
    this.padding,
    this.text,
  });

  final GSFieldType fieldType;
  final double width;
  final EdgeInsets padding;
  final String text;

  @override
  Widget build(BuildContext context) {
    Widget field;

    switch (fieldType) {
      case GSFieldType.cep:
        field = _buildCepField(text: text ?? 'CEP');
        break;
      case GSFieldType.cpf:
        field = _buildCnpjCpfField(
          text: text ?? 'CPF',
          mask: _maskCpf,
        );
        break;
      case GSFieldType.cnpj:
        field = _buildCnpjCpfField(
          text: text ?? 'CNPJ',
          mask: _maskCnpj,
        );
        break;
      case GSFieldType.cnpjCpf:
        field = _buildCnpjCpfField(
          text: text ?? 'CNPJ ou CPF',
          mask: _maskCpf,
        );
        break;
      case GSFieldType.phone:
        field = _buildPhoneField(
          text: text ?? 'Telefone',
        );
        break;
    }

    return field;
  }

  Widget _buildCnpjCpfField({String text, String mask}) {
    final MaskedTextController controller = MaskedTextController(mask: mask);

    if (fieldType == GSFieldType.cnpjCpf) {
      controller.beforeChange = (String previous, String next) {
        if (next.length == 14) {
          controller.updateMask(_maskCpf);
        } else if (next.length == 15) {
          controller.updateMask(_maskCnpj);
        }
        return true;
      };
    }

    return Container(
      width: width ?? 180.0,
      padding: padding,
      child: TextField(
        controller: controller,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          labelText: text,
          border: OutlineInputBorder(),
        ),
      ),
    );
  }

  Widget _buildPhoneField({String text}) {
    final MaskedTextController controller = MaskedTextController(mask: _maskPhone);

    return Container(
      width: width ?? 195.0,
      padding: padding,
      child: TextField(
        controller: controller,
        keyboardType: TextInputType.phone,
        decoration: InputDecoration(
          prefixIcon: Icon(Icons.phone),
          labelText: text,
          border: OutlineInputBorder(),
        ),
      ),
    );
  }

  Widget _buildCepField({String text}) {
    final MaskedTextController controller = MaskedTextController(mask: _maskCep);

    return Container(
      width: width ?? 180.0,
      padding: padding,
      child: TextField(
        controller: controller,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          labelText: text,
          border: OutlineInputBorder(),
        ),
      ),
    );
  }
}
