import 'package:flutter/material.dart';

class GSCombobox extends StatefulWidget {
  const GSCombobox({
    @required this.labelText,
    @required this.values,
    Key key,
  }) : super(key: key);

  final String labelText;
  final List<dynamic> values;

  @override
  State<StatefulWidget> createState() => _GSComboboxState();
}

class _GSComboboxState extends State<GSCombobox> {
  List<DropdownMenuItem<String>> _dropDownMenuItems;
  String _currentValue;

  @override
  void initState() {
    super.initState();
    _dropDownMenuItems = getDropDownMenuItems();
    _currentValue = _dropDownMenuItems[0].value;
  }

  @override
  Widget build(BuildContext context) {
    return DropdownButtonFormField<dynamic>(
      value: _currentValue,
      items: _dropDownMenuItems,
      onChanged: changedDropDownItem,
      decoration: InputDecoration(
        labelText: widget.labelText,
        border: OutlineInputBorder(),
      ),
    );
  }

  List<DropdownMenuItem<String>> getDropDownMenuItems() {
    final List<DropdownMenuItem<String>> items = <DropdownMenuItem<String>>[];
    for (String value in widget.values) {
      items.add(
        DropdownMenuItem<String>(
          value: value,
          child: Text(value),
        ),
      );
    }
    return items;
  }

  void changedDropDownItem(String selectedValue) {
    setState(() {
      _currentValue = selectedValue;
    });
  }
}
