import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';

class GSDatePicker extends StatelessWidget {

  GSDatePicker({
    this.width,
    this.padding,
    this.labelText,
  });

  final double width;
  final EdgeInsets padding;
  final String labelText;
  final DateFormat format = DateFormat('dd MMM yyyy');

  @override
  Widget build(BuildContext context) {
    return Container(
        width: width ?? 203.0,
        padding: padding,
        child: DateTimeField(
          keyboardType: TextInputType.datetime,
          decoration: InputDecoration(
            labelText: labelText,
            border: OutlineInputBorder(),
            prefixIcon: Icon(Icons.date_range),
          ),
          format: format,
          onShowPicker: (BuildContext context, DateTime currentValue) {
            return showDatePicker(
              context: context,
              firstDate: DateTime(1900),
              initialDate: currentValue ?? DateTime.now(),
              lastDate: DateTime(2100),
            );
          },
        ));
  }
}
