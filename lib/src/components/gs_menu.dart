import 'package:flutter/material.dart';

class Menu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          AppBar(
            automaticallyImplyLeading: false,
            title: Image.asset(
              'assets/images/geosales_logo_150px.png',
              width: 148.0,
              height: 32.0,
              fit: BoxFit.contain,
            ),
            actions: <Widget>[
              IconButton(
                icon: Icon(Icons.close),
                color: Colors.white,
                onPressed: () {
                  Navigator.pop(context);
                },
              )
            ],
          ),
          ListTile(
            title: const Text('Início'),
            onTap: () => Navigator.pushReplacementNamed(context, '/home'),
          ),
          ListTile(
            title: const Text('Atendimento ao Cliente'),
            onTap: () => Navigator.pushReplacementNamed(context, '/atendimento'),
          ),
          ListTile(
            title: const Text('Cadastro de Cliente'),
            onTap: () => Navigator.pushReplacementNamed(context, '/clienteCadastro'),
          ),
          ListTile(
            title: const Text('Agenda'),
            onTap: () => Navigator.pushReplacementNamed(context, '/agenda'),
          ),
        ],
      ),
    );
  }
}